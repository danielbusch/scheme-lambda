#!/usr/bin/chicken-csi -s

(newline)
(display "  ¯\\_(ツ)_/¯  ") (newline)
(newline)

; no need for if

(display " -- OUR OWN IF") (newline)

(define mad-true (lambda (consequence alternative) (consequence)))
(define mad-false (lambda (consequence alternative) (alternative)))

(define (mad-if boolean consequence alternative)
  (boolean consequence alternative))

(mad-if mad-false
  (lambda () (display "5") (newline))
  (lambda () (display "10") (newline)))

(newline)

; moving exit
(exit)

; no need for pairs

(display " -- OUR OWN PAIRS") (newline)

(define (mad-cons x y)
  (lambda (m)
    (mad-if m
      (lambda () x)
      (lambda () y))))
(define (mad-car z) (z mad-true))
(define (mad-cdr z) (z mad-false))

(define mad-pair (mad-cons 2 3))

(display (mad-car mad-pair)) (newline)
(display (mad-cdr mad-pair)) (newline)

(newline)

; no need for (natural) numbers

(display " -- OUR OWN NUMBERS") (newline)

(define zero (lambda (f) (lambda (x) x)))
; (define one (lambda (f) (lambda (x) (f x))))
; (define two (lambda (f) (lambda (x) (f (f x)))))
; (define three (lambda (f) (lambda (x) (f (f (f x))))))
; ...

(define (succ n)
  (lambda (f) (lambda (x) (f ((n f) x)))))

(define one (succ zero))
(define two (succ one))
(define three (succ two))

(define (add a b)
  (lambda (f) (lambda (x) ((a f) ((b f) x)))))

(define (make-mad-false x) mad-false)
(define (is-zero? n) ((n make-mad-false) mad-true))

; ((zero make-mad-false) mad-true)
; = (((lambda (f) (lambda (x) x)) make-mad-false) mad-true)
; = ((lambda (x) x) mad-true)
; = mad-true
 
; ((one make-mad-false) mad-true)
; = (((lambda (f) (lambda (x) (f x))) make-mad-false) mad-true)
; = ((lambda (x) (make-mad-false x)) mad-true)
; = (make-mad-false mad-true)
; = mad-false

(mad-if (is-zero? zero)
  (lambda () (display "zero is zero") (newline))
  (lambda () (display "zero is not zero") (newline)))

(mad-if (is-zero? one)
  (lambda () (display "one is zero") (newline))
  (lambda () (display "one is not zero") (newline)))

(mad-if (is-zero? (add one two))
  (lambda () (display "three is zero") (newline))
  (lambda () (display "three is not zero") (newline)))

; well... let's just take this for granted now
(define (pred n)
  (lambda (f) (lambda (x)
    (((n (lambda (g) (lambda (h) (h (g f))))) (lambda (u) x)) (lambda (u) u)))))

(mad-if (is-zero? (pred (pred two)))
  (lambda () (display "is zero") (newline))
  (lambda () (display "is not zero") (newline)))

(newline)

; no one needs a loop

(display " -- OUR OWN LOOP") (newline)

(define (for_ n f dummy)
  (mad-if (is-zero? n)
    (lambda () (f n))
    (lambda () (for_ (pred n) f (f n)))))
(define (for n f)
  (for_ n f zero))

(for three (lambda (i) (display "hi × "))) (newline)

(newline)

; actually we don't even need recursion in the first place:
;
; Take the strict fixed point combinator
;   Z = λf.(λx.f(λv.xxv))(λx.f(λv.xxv))
; and apply it to some function R
;   ZR = (λx.R(λv.xxv))(λx.R(λv.xxv))
;      = R(λv.(λx.R(λv.xxv))(λx.R(λv.xxv))v)
;      = R(λv.(ZR)v)
;      = λv.R(ZR)v
;
; If R produces a function, which takes the argument v, R
; applied to (ZR) reduces to that function and computes
; whatever it should with v.

(display " -- OUR OWN RECURSION MECHANISM") (newline)

(define (Z f)
  ((lambda (x) (f (lambda (v) ((x x) v))))
   (lambda (x) (f (lambda (v) ((x x) v))))))

(define (R sum)
  (lambda (n)
    (mad-if (is-zero? n)
      (lambda () zero)
      (lambda () (add n (sum (pred n)))))))

(define six ((Z R) three))
(for six (lambda (i) (display "hi × "))) (newline)

(newline)

